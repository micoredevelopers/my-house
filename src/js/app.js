require('./helpers')

const id = $('main').attr('id')

$(window).on('load', function () {
  $('.bingc-phone-button-tooltip').text(`Мы Вам перезвоним в ближайшее время!`)
  
  $('.bingc-phone-button ').on('click', function () {
    $('.bingc-we-will-call-you-restriction').html(`Мы Вам перезвоним в ближайшее время!`)
  })
  
  AOS.init({
    startEvent: 'DOMContentLoaded',
    initClassName: 'aos-init',
    animatedClassName: 'aos-animate',
    offset: 0,
    duration: 1200,
    easing: 'motion-easing',
    once: true,
    mirror: true
  })
  
  new Blazy({
    breakpoints: [{
      width: 576,
      src: 'data-src-small'
    }],
    success: function (element) {
      setTimeout(function () {
        var parent = element.parentNode
        
        parent.className = parent.className.replace(/\bloading\b/, '')
      }, 200)
    }
  })
})

switch (id) {
  case 'main-page':
    require('./pages/index.js')
    break
}
