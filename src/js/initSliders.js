const gallerySlider = $('.gallery-slider')
const mainSlider = $('.main-section__slider')
const pricesSlider = $('.prices-section__slider')
const reviewsSlider = $('.reviews-section__reviews-slider')
const mainSliderNav = $('.main-section__slider-wrap .slider-nav')

const initGallerySlider = () => {
  gallerySlider.on('init', function (e, { slideCount, currentSlide }) {
    const all = `${slideCount < 10 ? '0': ''}${slideCount}`
    const current = `${currentSlide < 10 ? '0': ''}${currentSlide + 1}`
    
    $('.gallery-slider-pag .slider-pag__text_all').text(all)
    $('.gallery-slider-pag .slider-pag__text_current').text(current)
  })
  
  gallerySlider.slick({
    dots: false,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '.gallery-slider-prev',
    nextArrow: '.gallery-slider-next'
  })
  
  gallerySlider.on('afterChange', function (e, { currentSlide }) {
    const current = `${currentSlide < 10 ? '0': ''}${currentSlide + 1}`
    
    $('.gallery-slider-pag .slider-pag__text_current').text(current)
  })
}

const destroyGallerySlider = () => {
  gallerySlider.slick('unslick')
}

const initReviewsSlider = () => {
  reviewsSlider.on('init', function (e, { slideCount, currentSlide }) {
    const all = `${slideCount < 10 ? '0': ''}${slideCount}`
    const current = `${currentSlide < 10 ? '0': ''}${currentSlide + 1}`
    
    $('.reviews-slider-pag .slider-pag__text_all').text(all)
    $('.reviews-slider-pag .slider-pag__text_current').text(current)
  })
  
  reviewsSlider.slick({
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.preview-slider-prev',
    nextArrow: '.preview-slider-next',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })
  
  reviewsSlider.on('afterChange', function (e, { currentSlide }) {
    const current = `${currentSlide < 10 ? '0': ''}${currentSlide + 1}`
    
    $('.reviews-slider-pag .slider-pag__text_current').text(current)
  })
}

const initMainSlider = () => {
  mainSlider.on('init', function (e, { slideCount, currentSlide }) {
    const slide = mainSlider.find('.slide')
    const oneSlide = mainSliderNav.width() / slideCount
    const all = `${slideCount < 10 ? '0': ''}${slideCount}`
    const current = `${currentSlide < 10 ? '0': ''}${currentSlide + 1}`
    
    setTimeout(function () {
      slide.addClass('with-delay')
      mainSlider.addClass('start-scale')
    }, 1600)
    mainSliderNav.find('.slider-nav__progress').width(oneSlide)
    
    $('.main-slider-pag .slider-pag__text_all').text(all)
    $('.main-slider-pag .slider-pag__text_current').text(current)
  })
  
  mainSlider.slick({
    fade: true,
    dots: false,
    speed: 1200,
    // arrows: false,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 7000,
    prevArrow: '.main-slider-prev',
    nextArrow: '.main-slider-next'
  })
  
  mainSlider.on('beforeChange', function (e,  { slideCount }, currentSlide, nextSlide) {
    const current = `${nextSlide < 10 ? '0': ''}${nextSlide + 1}`
    const oneSlide = mainSliderNav.width() / slideCount
    
    $('.main-slider-pag .slider-pag__text_current').text(current)
    mainSliderNav.find('.slider-nav__progress').width(oneSlide * (nextSlide + 1))
    
    mainSlider.addClass('end-animation')
    mainSlider.removeClass('start-animation')
  })
  mainSlider.on('afterChange', function () {
    mainSlider.addClass('start-animation')
    mainSlider.removeClass('end-animation')
  })
}

const initPricesSlider = () => {
  pricesSlider.slick({
    dots: false,
    arrows: false,
    slidesToShow: 3,
    draggable: false,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })
}

export { initGallerySlider, initReviewsSlider, initMainSlider, initPricesSlider, destroyGallerySlider }
