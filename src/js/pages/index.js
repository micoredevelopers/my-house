import { initMainSlider, initPricesSlider, initReviewsSlider } from '../initSliders'

$(document).ready(function () {
  const collapseBox = $('.object-collapse__body')
  const objectGalleryBtn = $('.object-card__gallery-btn')
  const blockWidth = Math.trunc(1140 * 0.33333)
  const offsetWidth = Math.trunc(($(window).width() - 1140) / 2)
  const allWidth = (offsetWidth + blockWidth) - 15
  
  if ($(window).width() > 991) {
    $('.main-section__bottom-info').width(allWidth)
    $('.services-section__image').css({ width: `calc(100% + ${offsetWidth - 60}px)`})
    $('.services-section__image.even-image').css({ transform: `translateX(-${offsetWidth - 60}px)`})
  }
  
  collapseBox.on('shown.bs.collapse', function () {
    $(this).find('.object-scroll').addClass('overflow-auto')
  })
  collapseBox.on('hidden.bs.collapse', function () {
    $(this).find('.object-scroll').removeClass('overflow-auto')
  })
  
  initMainSlider()
  initPricesSlider()
  initReviewsSlider()
  
  objectGalleryBtn.on('click', function () {
    const title = $(this).attr('data-title')
    
    $('#gallery-modal').find('#modal-title').text(title)
  })
})
