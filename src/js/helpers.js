import { destroyGallerySlider, initGallerySlider } from './initSliders'

const siteHeader = $('.header')
const galleryModal = $('#gallery-modal')
const successAlert = `<div class="alert fade show alert-success" role="alert">Сообщение успешно отправлено!</div>`
const errorAlert = `<div class="alert fade show alert-danger" role="alert">Произошла ошибка при отправке сообщения!</div>`

$('input[type=tel]').mask('+38 (099) 999-99-99')

const animateScroll = (offset, node) => {
  $('html, body').animate({
    scrollTop: $($.attr(node, 'href')).offset().top - offset
  }, 700)
}


const convertImages = (query, callback) => {
  const images = document.querySelectorAll(query)

  images.forEach(image => {
    fetch(image.src)
      .then(res => res.text())
      .then(data => {
        const parser = new DOMParser()
        const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('svg')

        if (svg) {
          if (image.id) svg.id = image.id
          if (image.className) svg.classList = image.classList
          image.parentNode.replaceChild(svg, image)
        }
      })
      .then(callback)
      .catch(error => console.error(error))
  })
}

const generateSlide = src => `
  <div class="slide">
    <div class="gallery-image">
      <img src="${src}" alt="Object">
    </div>
  </div>
`

const toggleMenu = () => {
  $('.global-menu').toggleClass('open')
  $('html, body').toggleClass('overflow-hidden')
}

convertImages('.svg')

$(window).on('scroll', function () {
  const trigger = 90
  let top = Math.trunc($(window).scrollTop())

  if (top > trigger) {
    siteHeader.addClass('active menu-open')
  } else {
    siteHeader.removeClass('active menu-open')
  }
})

$(document).on('click', 'a[href^="#"]', function (e) {
  e.preventDefault()

  if ($(window).width() < 991) {
    $('.global-menu').removeClass('open')
    $('html, body').removeClass('overflow-hidden')

    setTimeout(() => {
      animateScroll(80, this)
    }, 400)
  } else {
    animateScroll(60, this)
  }
})

$('.header__menu-wrap').on('click', function () {
  if ($(this).hasClass('open')) {
    /* Menu close */
    $(this).removeClass('open')
    !siteHeader.hasClass('active') && siteHeader.removeClass('menu-open')
  } else {
    /* Menu open */
    $(this).addClass('open')
    !siteHeader.hasClass('active') && siteHeader.addClass('menu-open')
  }

  toggleMenu()
})

$('#feedback-modal').on('hidden.bs.modal', function () {
  const modal = $(this)

  modal.find('input, textarea').val('')
})

$('#feedback-form').on('submit', function (e) {
  let errors = {}
  const form = $(this)
  const url = form.attr('action')
  const data = form.serializeArray()
  const preLoader = form.find('.lds-ring')
  const inputs = form.find('input, textarea')
  const inputsValidation = form.find('input:not([type="hidden"])')
  e.preventDefault()

  inputsValidation.each((index, input) => {
    const name = $(input).attr('name')
    const val = $(input).val()

    if (val === '') {
      $(input).parent().addClass('error')
      $(input).parent().append(`<small>Поле не должно быть пустым</small>`)

      errors[name] = val
    } else {
      $(input).parent().removeClass('error')
      $(input).parent().find('small').remove()
    }
  })

  if (Object.keys(errors).length === 0) {
    preLoader.removeClass('d-none')

    $.ajax({ url, data, method: 'POST' })
      .done(function (res) {
        form.prepend(successAlert)
        preLoader.addClass('d-none')
        inputs.val('')
      })
      .fail(function (res) {
        form.prepend(errorAlert)
        preLoader.addClass('d-none')
      })

    setTimeout(function () {
      $('.alert').alert('close')
    }, 2500)
  }
})

galleryModal.on('show.bs.modal', function () {
  setTimeout(initGallerySlider, 200)
})

galleryModal.on('hidden.bs.modal', function () {
  destroyGallerySlider()
})

$('[data-target="#gallery-modal"]').on('click', function () {
  const gallery = $(this).attr('data-gallery').split(',')
  const slider = galleryModal.find('.gallery-slider')

  slider.html('')

  gallery.map(src => {
    slider.append(generateSlide(src))
  })
})

$('[data-target="#feedback-modal"]').on('click', function () {
  const section = $(this).attr('data-section')

  $('#feedback-modal').find('input[type=hidden]').val(section)
})

$('[data-toggle="modal"]').on('click', function () {
  $('html').addClass('overflow-hidden')
})

$('.modal').on('hide.bs.modal', function () {
  $('html').removeClass('overflow-hidden')
})
