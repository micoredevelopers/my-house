<?php


use Psr\Http\Message\ServerRequestInterface;
use Slim\Factory\AppFactory;
use Symfony\Component\Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__.'/../.env');

$app = AppFactory::create();
if ('dev' === $_ENV['APP_ENV']){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    $errorMiddleware = $app->addErrorMiddleware(true, true, true);
    $errorMiddleware->setDefaultErrorHandler(function (ServerRequestInterface $request, Throwable $exception
    ) use ($app) {
        $payload = ['msg' => $exception->getMessage()];
        $response = $app->getResponseFactory()->createResponse(422);
        $response->getBody()->write(
            json_encode($payload, JSON_UNESCAPED_UNICODE)
        );
      return $response;
    });
}


$container = new App\Container(__DIR__.'/..');

AppFactory::setContainer($container);

$app->addBodyParsingMiddleware();

$appHandler = include(__DIR__.'/app.php');
$appHandler($app, $container);

$app->run();
