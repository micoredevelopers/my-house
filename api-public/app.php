<?php

namespace App;

use App\MailerService\FeedbackMessage;
use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

return function (App $app, Container $container){


    $baseUri = $container->getParam('base_url');

    $app->post($baseUri.'/feedback', function (Request $request, Response $response, $args) use ($container) {

        $emailService = $container->getMailerService();
        $message = FeedbackMessage::create($request->getParsedBody());
        $emailService->sendFeedback($message);

        $response->getBody()->write(
            json_encode(['msg' => 'Success'], JSON_UNESCAPED_UNICODE)
        );

        return $response;
    });
};
