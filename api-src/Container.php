<?php


namespace App;


use App\MailerService\MailerService;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport;

class Container extends \DI\Container
{

    private $params = [];
    /**
     * @var string
     */
    private $projectDir;

    public function __construct(string $projectDir)
    {
        parent::__construct(null, null, null);
        $this->projectDir = $projectDir;
        $this->boot();
    }


    /**
     * @return MailerService
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function getMailerService(): MailerService
    {
        return $this->get(MailerService::class);
    }

    public function getParam(string $key)
    {
        return $this->params[$key] ?? null;
    }

    private function boot(): void
    {

        $this->params = [
            'admin_email' => $_ENV['ADMIN_EMAIL'] ?? null,
            'server_email' => $_ENV['SERVER_EMAIL'] ?? null,
            'resource_path' => $this->projectDir.'/api-resource',
            'base_url' => $_ENV['BASE_URI'] ?? '/',
            'app_env' => $_ENV['APP_ENV']
        ];

        $this->set(MailerInterface::class, function(){
           return new Mailer(Transport::fromDsn($_ENV['MAILER_DSN']));
        });

        $this->set(MailerService::class, function (){
            return new MailerService(
                $this->get(MailerInterface::class),
                $this->getParam('server_email'),
                $this->getParam('admin_email'),
                $this->getParam('resource_path')
            );
        });
    }

}
