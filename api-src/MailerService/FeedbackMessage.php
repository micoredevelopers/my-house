<?php


namespace App\MailerService;


class FeedbackMessage
{

    public $sectionFrom;
    public $name;
    public $phone;
    public $message;

    public function validate()
    {
        return
            (is_string($this->name) || is_null($this->name)) &&
            (is_string($this->phone) || is_null($this->phone)) &&
            (is_scalar($this->sectionFrom)) &&
            (is_scalar($this->message))
            ;
    }

    public static function create($data): self
    {
        $result = new self();
        if (is_array($data)) {
            foreach (get_class_vars(self::class) as $attribute => $defaultValue) {
                $result->$attribute = $data[$attribute] ?? null;
            }
        }
        return $result;
    }

}
