<?php


namespace App\MailerService;


use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerService
{

    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var string
     */
    private $serverEmail;
    /**
     * @var string
     */
    private $adminEmail;
    /**
     * @var string
     */
    private $resourcePath;

    /**
     * MailerService constructor.
     * @param MailerInterface $mailer
     * @param string $serverEmail
     * @param string $adminEmail
     * @param string $resourcePath
     */
    public function __construct(MailerInterface $mailer, string $serverEmail, string $adminEmail, string $resourcePath)
    {

        $this->mailer = $mailer;
        $this->serverEmail = $serverEmail;
        $this->adminEmail = $adminEmail;
        $this->resourcePath = $resourcePath;
    }

    /**
     * @param FeedbackMessage $feedbackMessage
     * @throws DataIsNotValid
     * @throws TransportExceptionInterface
     */
    public function sendFeedback(FeedbackMessage $feedbackMessage)
    {

        if (!$feedbackMessage->validate()){
            throw new DataIsNotValid();
        }

         $vars = get_object_vars($feedbackMessage);
         $body = file_get_contents($this->resourcePath.'/email-template.html');

        $body = str_replace(array_map(function($property){ return '{'.$property.'}';}, array_keys($vars)), array_values($vars), $body);

         $message = new Email();
         $message
             ->to($this->adminEmail)
             ->from($this->serverEmail)
             ->subject('Feedback')
             ->html($body)
             ->setBody()
         ;
          $this->mailer->send($message);
    }

}
