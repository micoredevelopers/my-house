<?php


namespace App\MailerService;


use Throwable;

class DataIsNotValid extends \Exception
{

    public function __construct($code = 0, Throwable $previous = null)
    {
        parent::__construct('Data is not valid', $code, $previous);
    }

}
