const CssNano = require('cssnano')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = () => new OptimizeCssAssetsPlugin({
  canPrint: false,
  cssProcessor: CssNano,
  assetNameRegExp: /\.css$/g,
  cssProcessorPluginOptions: {
    preset: ['default', { discardComments: { removeAll: true } }],
  }
})
